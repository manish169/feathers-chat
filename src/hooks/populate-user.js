// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
/* eslint-disable require-atomic updates */
// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {
    const { app, method, result, params } = context;

    const addUser = async message => {
      const user = await app.service('users').get(message.userId, params)
      return {
        ...message, // spread operator which is used to copy the value of an entire array into another
        user
      };
    };

    if (method === 'find') {
     result.data = await Promise.all(result.data.map(addUser)) 
    } else {
      context.result = await addUser(result)
    }

    return context; 
  };
};
